<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('advertiser')->nullable();
            $table->string('invoice')->nullable();
            $table->boolean('check')->default(false);
            $table->integer('check_number');
            $table->datetime('paid')->nullable();
            $table->date('month');
            $table->integer('reference');
            $table->decimal('amount',15,2);
            $table->integer('agency_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checks');
    }
}
