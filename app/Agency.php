<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable = ['name','commission'];


    public function checks()
    {
        return $this->hasMany('App\Check');
    }
}
