<?php 
namespace App\Herring\DataTypes;



class Money {
    private $pennies;

    private function __construct($pennies)
    {
        $this->pennies = (integer) $pennies;

    }

    public static function fromDollars($dollars)
    {
        return new static($dollars * 100);
    
    }

    public static function fromPennies($pennies)
    {
        return new static($pennies);
    }

    public  function inPennies()
    {
        return (string) $this->pennies;
    }

    public function inDollars()
    {
        return (string) $this->pennies;
    }

    public function inDollarsAndPennies()
    {
        return number_format($this->pennies/100,2);
    }
}