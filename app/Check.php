<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    protected $guarded = [];

    protected $dates = [
        'paid',
        'month'
    ];



    public function humanPaid()
    {
        return $this->paid->format('m/d/Y');
    }

    public function shortMonth()
    {
        return $this->month->format('M-d');
    }

    public function humanCheck()
    {
        return ($this->check?'Yes':'No');
    }

    public function agency()
    {
        return $this->belongsTo('App\Agency');
    }


}
