<?php

namespace App\Http\Controllers\API;

use App\Check;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Herring\DataTypes\Money;
use App\Http\Controllers\Controller;
use App\Http\Resources\CheckResource;
use App\Http\Requests\StoreCheckRequest;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return CheckResource::collection(Check::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCheckRequest $request)
    {
        $check = Check::create([
            'advertiser' => $request->advertiser,
            'invoice' => $request->invoice,
            'check' => (isset($request->check)?$request->check:0),
            'check_number' => $request->check_number,
            'paid' => $request->paid. " 12:00:00",
            'month' => $request->month . " 12:00:00",
            'reference' => $request->reference,
            'amount' => Money::fromDollars($request->amount)->inDollars(),
            'agency_id' => $request->agency,

        ]);

        return  new CheckResource($check);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $check = Check::find($id);

        return new CheckResource($check);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Check $check)
    {
        $check->advertiser = $request->get('advertiser',$check->advertiser);
        $check->invoice = $request->get('invoice',$check->invoice);
        $check->check = $request->get('check',$check->check);
        $check->check_number = $request->get('check_number',$check->check_number);
        $check->paid = $request->get('paid',$check->paid);
        $check->month = $request->get('month',$check->month);
        $check->reference = $request->get('reference',$check->reference);
        $check->amount = $request->get('amount',$check->amount);
        $check->agency_id = $request->get('agency',$check->agency_id);

        $check->save();

        return new CheckResource($check);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Check $check)
    {
        $check->delete();

        return response(null,204);
    }


    public function search(Request $request)
    {
        if($request->has('type'))
        {
            if($request->type === 'monthly')
            {
                
                $month = $request->month;
                $year = $request->has('year')?$request->year:Carbon::now()->year;
                $checks = Check::whereMonth('paid',$month)->whereYear('paid',$year)->get();
                return  CheckResource::collection($checks);
            }else {
                $checks = Check::all();
                return CheckResource::collection($checks);
            }
        }

        return [];
    }
}
