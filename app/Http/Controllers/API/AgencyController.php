<?php

namespace App\Http\Controllers\API;

use App\Agency;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AgencyResource;
use App\Http\Resources\AgencyCollection;
use App\Http\Requests\StoreAgencyRequest;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AgencyResource::collection(Agency::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgencyRequest $request)
    {
        $agency = Agency::create([
            'name' => $request->name,
            'commission' => $request->commission
        ]);


        return new AgencyResource($agency);       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $agency = null;
        if($request->has(['to','from'])){
            $agency = Agency::where('id',$id)->with(['checks'=> function($query) use ($request){
                $query->whereBetween('paid',[$request->input('from'),$request->input('to')])->get();
            }])->first();
        }else if( $request->has('month')){
            $agency = Agency::where('id',$id)->with(['checks' => function($query) use ($request) {
                $query->whereMonth('paid',$request->input('month'))->get();
            }])->first();
        }else {
            $agency = Agency::find($id);    
        }
        



        return new AgencyResource($agency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {
        $agency->name = $request->get('name',$agency->name);
        $agency->commission = $request->get('commission',$agency->commission);

        $agency->save();

        return new AgencyResource($agency);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agency $agency)
    {
        //

        $agency->delete();

        return response(null,204);
    }
}
