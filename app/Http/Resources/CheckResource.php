<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CheckResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'advertiser' => $this->advertiser,
            'invoice' => $this->invoice,
            'check' => $this->humanCheck(),
            'check_number' => $this->check_number,
            'paid' => $this->humanPaid(),
            'month' => $this->shortMonth(),
            'reference' => $this->reference,
            'amount' => $this->amount,
            'agency' => $this->agency,
           
        ];
    }
}
