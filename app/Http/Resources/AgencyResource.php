<?php

namespace App\Http\Resources;

use App\Http\Resources\CheckResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AgencyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'commission' => $this->commission,
            'checks' => CheckResource::collection($this->checks),
        ];
    }
}
